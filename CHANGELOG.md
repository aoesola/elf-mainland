# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).
## [1.0.8] 2022 01 26
更新 qqnet 广告的android 依赖
## [1.0.5] 2022 01 26
更新 微信支付的包名
## [1.0.5] 2022 01 26
移除 unity ads 库
移除facebook provider
## [1.0.2] 2022 01 25
修改talking data 的key
增加 支付的 loading bar
## [1.0.0] 2021 12 20
首个内地版本

## [0.1.0] - 2021-03-19

### This is the first release of *\<MergeMainland\>*.

*Short description of this release*
